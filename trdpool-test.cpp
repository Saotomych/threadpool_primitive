/// Unit test is gcd calculate of sections
#include <boost/date_time/posix_time/posix_time.hpp>
#include "tp.hpp"
using namespace mythreadpool;

typedef unsigned int	u32;
typedef unsigned char	u08;

#define MAXSIZE 	100000

const u32 maxsize = MAXSIZE;
const u32 maxnum = 100000000;
u32 arr[MAXSIZE];

u32 sects[200000][2];
u32 *answ;

inline u32 gcd (u32 a, u32 b) {
	while (b) {
		a = a%b;
		std::swap(a,b);
	}
	
	return a;
}

bool gcd_sector(u32 ibgn, u32 iend, u32& answ)
{
	answ = 0;
	
	u32 maxdiv = arr[ibgn];
	if (!maxdiv) return false;

	for (u32 x = ibgn+1; x <= iend; ++x)
	{
		if (!arr[x]) return false;
		if (arr[x] % maxdiv) maxdiv = gcd(arr[x], maxdiv);
	}
	
	boost::this_thread::sleep(boost::posix_time::milliseconds(arr[ibgn]&0xff));
	answ = maxdiv;
	
	return true;
}

bool factorial(u32 n, u32& answ)
{
	if (n<=1) return true;
	factorial(n-1, answ);
	answ *= n;
	
	return true;
}

void RunSync(u32 size)
{
	// Run with main thread
	for (u32 i=0; i < size; ++i)
	{
		gcd_sector(sects[i][0], sects[i][1], answ[i]);
	}	
}

void RunAsync(u32 thrs, u32 size)
{
	// Run with thread pool
	CThreadPool tp(thrs, 100, 300);
	
	u32 i1 = size >> 2;
	u32 i2 = size >> 1;
	
	for (u32 i=0; i < i1; ++i)
	{
		while ( !tp.AddTask(i&7, boost::bind(gcd_sector, sects[i][0], sects[i][1], boost::ref(answ[i]))) )
		{
//			boost::this_thread::sleep(boost::posix_time::milliseconds(5));
		}
	}
	
	boost::this_thread::sleep(boost::posix_time::milliseconds(50));
	tp.SetThreads(5);
	
	for (u32 i=i1; i < i2; ++i)
	{
		while ( !tp.AddTask(i&7, boost::bind(gcd_sector, sects[i][0], sects[i][1], boost::ref(answ[i]))) )
		{
//			boost::this_thread::sleep(boost::posix_time::milliseconds(5));
		}
	}

	boost::this_thread::sleep(boost::posix_time::milliseconds(50));
	tp.SetThreads(15);

	for (u32 i=i2; i < (i2+i1); ++i)
	{
		while ( !tp.AddTask(i&7, boost::bind(gcd_sector, sects[i][0], sects[i][1], boost::ref(answ[i]))) )
		{
//			boost::this_thread::sleep(boost::posix_time::milliseconds(5));
		}
	}

	boost::this_thread::sleep(boost::posix_time::milliseconds(50));
	tp.SetThreads(10);

	for (u32 i=i2+i1; i < size; ++i)
	{
		sects[i][0] &= 0x1f;
		if (sects[i][0] < 3) sects[i][0] = 3;
		answ[i] = 1;
		while ( !tp.AddTask(i&7, boost::bind(factorial, sects[i][0], boost::ref(answ[i]))) )
		{
//			boost::this_thread::sleep(boost::posix_time::milliseconds(5));
		}
	}

	boost::this_thread::sleep(boost::posix_time::milliseconds(5));
	
	tp.DoTasksAndStop();
//	tp.ForceStop();
}

int main(int argn, char *args[])
{

	u32 n=0, kind, size;
	std::cin >> kind >> size;
	
	if (size > 200000) exit(0);
	
	if (kind) std::cin >> n;
	
	for (u32 i=0; i< maxsize; ++i) arr[i] = ((rand() % maxnum)+2) & 0xFFFFFFFE;
	
	for (u32 i=0; i< size; ++i)
	{
		int n1=1, n2=0;
		while (n1 > n2)
		{
			n1 = (rand() % (maxsize-3)+1);
			n2 = rand() % (maxsize-n1) + n1;
		}
		sects[i][0] = n1;
		sects[i][1] = n2;
	}

	answ = new(u32[size]);

	if (kind)
	{
		RunAsync(n, size);
		
		std::cout << "Calculate results with thread pool: ";
		for (u32 i=0; i<size; i++) std::cout << answ[i] << ", ";
		std::cout << std::endl;
	} else {
		RunSync(size);

		std::cout << "Calculate results with function's cycle: ";
		for (u32 i=0; i<size; i++) std::cout << answ[i] << ", ";
		std::cout << std::endl;
	}
	
	delete[] answ;
}


