#ifndef DEBUG_LOG
#define DEBUG_LOG

#include <iostream>
#include <boost/date_time/posix_time/posix_time.hpp>

class CLog
{
	bool ts;
	boost::posix_time::ptime bgntime;
	boost::mutex mut;
	boost::mutex tmut;

	CLog(): ts(true), bgntime(boost::posix_time::microsec_clock::local_time()) {}

public:

	static CLog& clog()
	{
		static CLog *l=NULL;
		static boost::mutex mut;

		boost::mutex::scoped_lock lock(mut);

		if (!l) l = new CLog();
		l->locklog();
		return *l;
	}

	CLog& locklog() {

#ifdef DEBUG
		mut.lock();
#endif
		return *this; 
	}

	typedef std::basic_ostream<char, std::char_traits<char> > CoutType;
	typedef CoutType& (*StandardEndLine)(CoutType&);
	
#ifdef DEBUG
	template <typename T>
	CLog& operator <<(const T& p)
	{
		boost::mutex::scoped_lock lock(tmut);

		if (ts)
		{
			ts = false;
			
			boost::posix_time::ptime t(boost::posix_time::microsec_clock::local_time());
			std::cerr << (t-bgntime) << ": ";
		}
		std::cerr << p;
		
		return *this;
	}
	
	CLog& operator <<(StandardEndLine e)
    {
		boost::mutex::scoped_lock lock(tmut);

		std::cerr << std::endl;
		std::cerr.flush();

		ts = true;
		mut.unlock();

		return *this;
    }
#else

	template <typename T>
	CLog& operator <<(const T& p)
	{
		return *this;
	}

	CLog& operator <<(StandardEndLine e)
    {
		return *this;
    }
#endif
	
};

#endif // DEBUG_LOG

