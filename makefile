CC=g++
CFLAGS=-c -Wall -O2
LDFLAGS=-l boost_thread
SOURCES=trdpool-test.cpp
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=tp-test

all: $(SOURCES) $(EXECUTABLE)
    
$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@
    
.cpp.o:
	$(CC) $(CFLAGS) $(SOURCES) -o $@

clean: 
	rm -rf *.o tp-test tp